;;; Full Functional Queue Implementation
;;; Copyright © 2019 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>

;;; Commentary:
;;
;; Full functional queue implementation.
;;
;;; Code:

(define-module (queues)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:export (queue
            queue?
            make-queue
            empty-queue?
            queue-val
            enq
            enq*
            deq
            deql
            queue-to-list
            queue-to-r-list
            mapq
            list-to-queue
            list-to-queue-r
            reverse-queue
            queue-equal?))

;;;
;;; Intern tools.
;;;

(define (error)
  "Throw a matching error."
  (throw 'queue "Matching type error."))

;;;
;;; Type.
;;;

(define-record-type queue
  (%make-queue in out val)
  queue?
  (in  queue-in)
  (out queue-out)
  (val queue-val))

;;;
;;; Primitive.
;;;

(define* (make-queue #:key
                     (input  '())
                     (output '())
                     (value  #f))
  "Make a new queue."
  (%make-queue input output value))

(define (empty-queue? q)
  "True if Q is an empty queue, else false."
  (match q
    (($ queue () () _) #t)
    (($ queue _  _  _)  #f)
    (_ (error))))

(define (enq q x)
  "Add X on queue Q."
  (match q
    (($ queue in out v)
     (%make-queue (cons x in) out v))
    (_ (error))))

(define (enq* q l)
  "Add L list on queue Q."
  (match q
    (($ queue in out v)
     (%make-queue (append (reverse l) in) out v))
    (_ (error))))

(define (deq q)
  "Dequeue Q, and return new queue."
  (match q
    (($ queue () () _) (make-queue))
    (($ queue in () _)
     (let ((rin (reverse in)))
       (%make-queue '() (cdr rin) (car rin))))
    (($ queue in out _)
     (%make-queue in (cdr out) (car out)))
    (_ (error))))

(define (deql q)
  "Dequeue Q, and return list with (value new-queue)."
  (let ((nq (deq q)))
    (list (queue-val nq) nq)))

(define (mapq f q)
  "Map with function F on queue Q."
  (define (aux q acc)
    (match q
      (($ queue () () _) acc)
      (($ queue _  _  _)
       (let* ((r  (deq q))
              (v  (queue-val r))
              (nv (f v)))
         (aux r (enq acc nv))))
      (_ (error))))
  (match q
    (($ queue _ _ _) (aux q (make-queue)))
    (_ (error))))

(define (reverse-queue q)
  "Reverse queue Q."
  (match q
    (($ queue () () _) q)
    (($ queue in () v) (%make-queue '() in v))
    (($ queue () out v) (%make-queue out '() v))
    ;; FIXME: Complexity
    (($ queue _ _ _) (list-to-queue (queue-to-r-list q)))
    (_ (error))))

(define (queue-equal? q1 q2)
  "True if queue is equal, else return false."
  (let ((nq1 (deq q1))
        (nq2 (deq q2)))
    (match `(,nq1 ,nq2)
      ((($ queue () () v1)
        ($ queue () () v2))
       (if (equal? v1 v2)
           #t #f))
      ((($ queue _  _  v1)
        ($ queue _  _  v2))
       (if (equal? v1 v2)
           (queue-equal? nq1 nq2)
           #f))
      (_ (error)))))

;;;
;;; List operation.
;;;

(define (queue-to-list q)
  "Return list from queue Q."
  (match q
    (($ queue () () _) '())
    (($ queue _  _  _)
     (let* ((r (deq q))
            (v (queue-val r)))
       (cons v (queue-to-list r))))
    (_ (error))))

(define (queue-to-r-list q)
  "Return the reverse list from Q."
  (define (aux q acc)
    (match q
      (($ queue () () _) acc)
      (($ queue _  _  _)
       (let* ((r (deq q))
              (v (queue-val r)))
         (aux r (cons v acc))))
      (_ (error))))
  (aux q '()))

(define (list-to-queue l)
  "Make queue from the list L."
  (make-queue #:output l))

(define (list-to-queue-r l)
  "Make queue from the reverse list L."
  (make-queue #:input l))
