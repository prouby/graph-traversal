;;; Full Functional Graph Implementation
;;; Copyright © 2019 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>

;;; Commentary:
;;
;; Full functional graph implementation.
;;
;;; Code:

(define-module (graph)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (queues)
  #:export (node
            node?
            make-node

            graph
            graph?
            make-graph))

;;;
;;; Node.
;;;

(define-record-type node
  (%make-node label value color first-h last-h)
  node?
  (label   node-label)
  (value   node-value)
  (color   node-color)
  (first-h node-first-h)
  (last-h  node-last-h))

(define* (make-node #:key
                    (label #t)
                    (value #f)
                    (color 'WHITE)
                    (first-h #f)
                    (last-h  0))
  (%make-node label value color first-h last-h))

;;;
;;; Graph.
;;;

(define-record-type graph
  (%make-graph nodes neighbours)
  graph?
  (nodes      graph-nodes)
  (neighbours graph-neighbours))

(define* (make-graph #:key
                     (nodes '())
                     (neightbours '()))
  (%make-graph nodes neightbours))

;;;
;;; Tools.
;;;

(define (search-node lst label)
  (match lst
    (() #f)
    (_ (match (car lst)
         (($ node l _ _ _ _)
          (if (equal? l label)
              (car lst)
              (search-node (cdr lst) label)))
         (_ (throw 'graph "Node matching"))))))

(define (search-non-visited-node lst label)
  (match lst
    (() #f)
    ((n . r)
     (match n
       (($ node l _ c _ _)
        (if (equal? l label)
            (if (equal? c 'WHITE)
                n
                #f)
            (search-non-visited-node r label)))
       (_ (throw 'graph "Node matching"))))))

(define (search-neightbours g label)
  (match g
    (($ graph nodes neightbours)
     (assoc-ref neightbours label))
    (_ (throw 'graph "Graph matching"))))

(define* (update-node n
                      #:key
                      (label #f)
                      (value #f)
                      (color #f)
                      (first-h #f)
                      (last-h  #f))
  (match n
    (($ node l v c fh lh)
     (make-node #:label   (or label l)
                #:value   (or value v)
                #:color   (or color c)
                #:first-h (or first-h fh)
                #:last-h  (or last-h  lh)))
    (_ (throw 'graph "Node matching"))))

(define* (update-graph g
                       #:key
                       (nodes #f)
                       (neightbours #f))
  (match g
    (($ graph no ne)
     (make-graph #:nodes       (or nodes no)
                 #:neightbours (or neightbours ne)))
    (_ (throw 'graph "Graph matching"))))

(define* (update-node-on-graph g n #:key
                               (value   #f)
                               (color   #f)
                               (first-h #f)
                               (last-h  #f))
  (let ((label (node-label n)))
    (match g
      (($ graph nodes neightbours)
       (update-graph g
                     #:nodes
                     (map (λ (n)
                            (if (equal? label (node-label n))
                                (update-node n
                                             #:value   value
                                             #:color   color
                                             #:first-h first-h
                                             #:last-h  last-h)
                                n))
                          (graph-nodes g))))
      (_ (throw 'graph "Graph matching")))))

(define (reverse-graph g)
  (define (transform x)
    (match x
      ((s . d)
       (map (λ (y) `(,y . (,s))) d))
      (_ '())))

  (define (append-transform l)
    (define (a-t-loop l acc)
      (match l
        (() acc)
        ((x . r)
         (a-t-loop r (append (transform x) acc)))))
    (a-t-loop l '()))

  (define (append-all l x)
    (define (append-all-loop l x acc)
      (match l
        (() acc)
        (_ (match (car l)
             ((s . ())
              (append-all-loop (cdr l) x acc))
             ((s . d)
              (if (equal? s x)
                  (append-all-loop (cdr l) x (append d acc))
                  (append-all-loop (cdr l) x acc)))))))
    (append-all-loop l x '()))

  (match g
    (($ graph nodes neightbours)
     (let ((new-neightbours (append-transform neightbours))
           (nodes-list (map node-label nodes)))
       (update-graph g
                     #:neightbours
                     (delete
                      #f
                      (map (λ (x)
                             (let ((n (append-all new-neightbours x)))
                               (if (equal? n '()) #f `(,x . ,n))))
                           nodes-list)))))
    (_ (throw 'graph "Graph matching"))))

;;;
;;; BFS
;;;

(define (bfs f g)
  "Do bfs search on G, start at first node of G nodes."
  (define (bfs-loop g q)
    (cond
     ((empty-queue? q)  g)
     (else
      (let* ((q       (deq q))
             (u       (queue-val q))
             (g       (update-node-on-graph g u #:color 'GREY))
             (nodes   (graph-nodes g))
             (neights (search-neightbours g (node-label u))))
        (f u)
        (if (not neights)
            (bfs-loop g q)
            (let* ((neights-nodes
                    (delete #f
                            (map (λ (x)
                                   (search-non-visited-node nodes x))
                                 neights)))
                   (q (enq* q neights-nodes)))
              (bfs-loop g q)))))))
  ;; BFS start
  (match g
    (($ graph nodes _)
     (let ((u (car nodes)))
       (bfs-loop g (make-queue #:input (list u)))))
    (_ (throw 'graph "Graph matching"))))

;;;
;;; DFS
;;;

(define (dfs f g)
  (define (neights-loop g u t n)
    (match n
      (()
       (let* ((t (1+ t))
              (g (update-node-on-graph g u #:color 'BLACK #:last-h t)))
         (list g t)))
      ((v . r)
       (match (dfs-loop g v t)
         ((ng nt)
          (neights-loop ng u nt r))))))

  (define (dfs-loop g u t)
    (let* ((t (1+ t))
           (g (update-node-on-graph g u #:color 'GREY #:first-h t)))
      (f u)
      (let* ((neights (search-neightbours g (node-label u)))
             (nodes   (graph-nodes g))
             (neights-nodes
              (if (not neights)
                  '()
                  (delete #f
                          (map (λ (x)
                                 (search-non-visited-node nodes x))
                               neights)))))
        (match (neights-loop g u t neights-nodes)
          ((g t)
           (list g t))))))
  ;; DFS start
  (match g
    (($ graph nodes _)
     (dfs-loop g (car nodes) 0))
    (_ (throw 'graph "Graph matching"))))

;;;
;;; Tests.
;;;

(define g1
  (make-graph
   #:nodes (list
            (make-node #:label 'a
                       #:value "Hello ")
            (make-node #:label 'b
                       #:value "from ")
            (make-node #:label 'c
                       #:value "Scheme ")
            (make-node #:label 'd
                       #:value "graph")
            (make-node #:label 'e
                       #:value "!"))
   #:neightbours '((a . (b c))
                   (b . (d))
                   (d . (e)))))

(define display-node
  (match-lambda
    (($ node l v c fh lh)
     (display (format #f "-> ~a ~a\n" l v)))
    (_ #t)))

;;; Display result
(newline)
(let ((g2 (bfs display-node g1)))
  (newline)
  (let ((rg1 (reverse-graph g1)))
    (bfs display-node rg1)
    (newline)
    (let ((g3 (dfs display-node g1)))
      (newline))))
